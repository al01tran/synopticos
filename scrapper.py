# coding: utf8

from configparser import ConfigParser
from elasticsearch import Elasticsearch, ElasticsearchException
from library import calibre, goodreads, database
from tqdm import tqdm
from time import sleep
from nltk.corpus import stopwords
#from nltk.stem import WordNetLemmatizer

import json
import random
import logging
import logging.config
import numpy as np
import os
import psycopg2
import spacy
import string
import re

# log initialisation
logging.config.fileConfig('./configuration/logging.ini')
logger = logging.getLogger(__name__)


# read config file
config = ConfigParser(os.environ)
config.read('./configuration/parameters.ini')

def wait(t=None):
    if t is None:
        wait = max(7, random.gauss(10, 20), random.gauss(15, 10))
    else:
        wait = t
    tqdm.write(f"wait {wait} seconds")
    sleep(wait)

def openbkcase():
    global config

    # postgresql connection
    conn = psycopg2.connect(
    host=config['postgresql']['host'],
    database=config['postgresql']['database'],
    user=config['postgresql']['user'],
    password=config['postgresql']['password']
    )
    conn.autocommit = True

    bkcase = database.Bookcase(conn)
    return(bkcase)

def webscrap(folder=None):
    """
    Web scrapping goodreads html pages
    folder : epub folder
    """
    global config

    db = openbkcase()
    gr_talker = goodreads.Talker(config['goodreads']['key'])

    if folder is None:
        folder = config['calibre']['path']
    snoopy = calibre.Snooper(folder)

    for f in tqdm(snoopy):
        # skip if book already in database
        searchsql = db.getbook(f.writer, f.title)
        if searchsql:
            logger.info(f"Skipped {f.writer}'s book : {f.title}")
            continue

        logger.info(f"Search informations for {f.writer}'s book : {f.title}")

        try:
            # request to goodreads
            search_string = f'{gr_talker.title_filt(f.title)} {f.writer}'
            search_results = gr_talker.search(search_string)

            wait()

            # in case of bad writer metadata
            if not search_results:
                reg_writer = re.search(r'(?<=Calibre/)[\w\s]*', f.path)
                if reg_writer:
                    path_writer = reg_writer.group()
                if path_writer != f.writer:
                    search_string = f'{gr_talker.title_filt(f.title)} {path_writer}'
                    search_results = gr_talker.search(search_string)

                    wait()
                
            # no results
            if not search_results:
                db.insert(
                    table = 'nosummary',
                    path_epub = f.path,
                    title_epub = f.title,
                    writer_epub = f.writer,
                    lang_epub = f.lang
                )
                logger.info(f"No results for {f.writer}'s book : {f.title}")
                continue

            # filter results
            # by creating a triple containint the Jaccard distance, index in list and the result itself
            # and taking the min distance if equal, the min index
            _dist_result, _idx_result, sel_result = min(map(
                lambda x : (snoopy.distance_title(f.title, x['title']), search_results.index(x), x),
                search_results))

            # get infos
            html_result = gr_talker.get_html(sel_result['goodreadsid'])

            tqdm.write(f"Search informations done")

            wait()

            # insert to database
            db.insert(
                id_epub = f.id,
                path_epub = f.path,
                title_epub = f.title,
                writer_epub = f.writer,
                lang_epub = f.lang,
                id_gr = sel_result['goodreadsid'],
                title_gr = sel_result['title'],
                writer_gr = sel_result['author'],
                img_gr = sel_result['img_url'],
                html_gr = html_result
                )

        except Exception:
            logger.exception(f"Failed data creation for\n{f.path}")

def es_index(es):
    """
    Elasticsearch index initialization.
    """
    mapping = {
        "mappings": {
            "properties": {
                "id": {
                    "type": "text",
                    "fields": {"raw": {"type": "keyword", "ignore_above": 256}}
                },
                "writer": {
                    "type": "text",
                    "fields": {"raw": {"type": "keyword"}}
                },
                "title": {
                    "type": "text",
                    "fields": {"raw": {"type": "keyword"}}
                },
                "plot": {
                    "type": "text",
                    "fields": {"text": {"type": "text"}}
                },
                "chapters": {
                    "type": "text",
                    "fields": {"text": {"type": "text"}}
                },
                "plot_bag": {
                    "type": "text",
                    "fields": {"text": {"type": "text"}}
                },
                "chapters_bag": {
                    "type": "text",
                    "fields": {"text": {"type": "text"}}
                },
            }
        }
    }

    try:
        es.indices.create(index = 'library', body = mapping, request_timeout=30)
        logger.info('Elastic library index created')
    except ElasticsearchException as err:
        if err.error == 'resource_already_exists_exception':
            pass
        else:
            raise ElasticsearchException(err.error)

def filtnlem(text):
    """
    Function to remove stopwords and lemmatize remaining words.
    text: String to process
    output: tuple (string of lemmas, array of spacy vectors)
    """
    # load lemmatizer
    nlp = spacy.load('en', disable=['parser', 'ner'])
    processed = nlp(text)

    # remove punctuation rules
    punctuation = string.punctuation + string.whitespace + '—\"“”’'
    transrules = str.maketrans('', '', punctuation)

    # issue with list comprehension, '"' was not removed
    # so we keep the old fashion way
    lemmas = list()
    vectors = list()
    for token in processed:
        if not token.is_stop and str(token).translate(transrules):
            lemmas.append(token.lemma_)
            vectors.append(token.vector)

    return(' '.join(lemmas), vectors)

def vect2byte(listarray):
    """
    Function to convert list of arrays to numpy bytes string
    listarray: list of numpy arrays to convert
    output: tuple (
        bytesarray: converted array to bytes string,
        dtype: original numpy dtype,
        shape: original numpy array shape
        )
    """

    if not listarray:
        byte = (b'', None, (None, None))
    else:
        # retrieve array properties
        dtype = listarray[0].dtype

        # flatten and convert to bytes
        nparray = np.array(listarray, dtype=dtype)
        flatarray = nparray.flatten()
        bytesarray = flatarray.tobytes()

        shape = nparray.shape
        byte = (bytesarray, dtype, shape)
    return(byte)

def byte2vect(bytestring, dtype, shape):
    """
    Function to convert back numpy bytes string to numpy array
    bytestring: numpy byte string
    dtype: numpy data type
    shape: numpy array shape
    output: numpy array
    """
    flatarray = np.fromstring(bytestring, dtype=dtype)
    np_vect = flatarray.reshape(shape)
    return(np_vect)

def prepro(folder=None):
    # objects creation
    bkcase = openbkcase()
    gr = goodreads.Talker(config['goodreads']['key'])
    es = Elasticsearch(config['elastic']['host'])

    # index creation
    es_index(es)

    # token en lemmatizer
    nlp = spacy.load('en', disable=['parser', 'ner'])

    for row in tqdm(bkcase):
        
        book = calibre.Reader(row['path_epub'])

        id = row['id']
        id_gr = row['id_gr']
        writer = row['writer_gr']
        title = row['title_gr']

        # # for debug
        # if id < 143:
        #     continue

        logger.info(f'preprocessing {id}')

        plot = gr.get_plot(row['html_gr'])

        # lemmatize text
        if plot:
            plot_lem, plot_vect = filtnlem(plot)
        chapters_lem,  chapters_vect= zip(*map(filtnlem, book.chapters))

        # plot_bytes = vect2byte(plot_vect)
        plot_bytes = vect2byte(plot_vect)
        chapters_bytes = map(vect2byte, chapters_vect)

        # save to databases
        if plot:
            try:
                bkcase.insert(
                    table = 'spacy',
                    id_library = id_gr,
                    part = 0,
                    dtype = str(plot_bytes[1]),
                    shape0 = plot_bytes[2][0],
                    shape1 = plot_bytes[2][1],
                    vector = plot_bytes[0]
                )
            except Exception as e:
                logger.warning(e)
        for i, byte in enumerate(chapters_bytes):
            try:
                bkcase.insert(
                    table = 'spacy',
                    id_library = id_gr,
                    part = i + 1,
                    dtype = str(byte[1]),
                    shape0 = byte[2][0],
                    shape1 = byte[2][1],
                    vector = byte[0]
                )
            except Exception as e:
                logger.warning(e)

        doc = {
            "id": id_gr,
            "writer": writer,
            "title": title,
            "plot": plot,
            "chapters": book.chapters,
            "plot_bag": plot_lem,
            "chapters_bag": chapters_lem
        }
    
        res = es.index(index="library", id=id, body=doc, request_timeout=60)
        logger.info(res)

if __name__=='__main__':
    logger.info('Begin run')
    prepro()
    logger.info('End run')

    

    # doc = {
    #     'author': 'kimchy',
    #     'text': 'Elasticsearch: cool. bonsai cool.',
    #     'timestamp': datetime.now(),
    # }
    # res = es.index(index="test-index", id=1, body=doc)
    # print(res['result'])

    # res = es.get(index="test-index", id=1)
    # print(res['_source'])

    # es.indices.refresh(index="test-index")

    # res = es.search(index="test-index", body={"query": {"match_all": {}}})
    # print("Got %d Hits:" % res['hits']['total']['value'])
    # for hit in res['hits']['hits']:
    #     print("%(timestamp)s %(author)s: %(text)s" % hit["_source"])

