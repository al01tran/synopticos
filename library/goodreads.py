# coding: utf8

import logging
import requests
import re
import xmltodict

from nltk.corpus import stopwords
from requests_html import HTMLSession, HTML

logger = logging.getLogger(__name__)

class Talker(object):
    """
    Talker interact with goodreads rest API.
    """

    def __init__(self, user_key):
        """
        user_key : the API user key for goodreas requests
        """
        self.key = user_key

    def search(self, string):
        """
        Search for books using title or writer name
        string : the input string search
        output : a list of books result
        """
        data = {
            "key" : self.key,
            "q" : string
        }

        resp = requests.get("https://www.goodreads.com/search.xml", params=data)
        result = []

        if resp.status_code == 200:
            # read the xml when we have an OK status
            resp_dict = xmltodict.parse(resp.text)
            resp_search = resp_dict['GoodreadsResponse']['search']

            if resp_search['query'] != string.replace('.', ''):
                logging.warning(f"Requested search {string} not equal to {resp_search['query']}")

            nb_result = int(resp_search['total-results'])
            if not nb_result:
                logging.info(f"No result for {string}")
            elif nb_result == 1:
                res = resp_search['results']['work']
                result.append(self.__fsearch(res))
            else:
                for res in resp_search['results']['work']:
                    result.append(self.__fsearch(res))

        else:
            logging.warning(f'Search result status code {resp.status_code}')
            
        return(result)

    def get_plot(self, rawhtml):
        """
        Retrieve infos from book page
        html : goodreads book html raw page
        output : plot string or None if empty
        """

        html = HTML(html=rawhtml)

        req_plot = html.find('#description>span')
        if req_plot:
            plot = str(req_plot[-1].full_text)
        else:
            plot = None
        return(plot)

    def get_html(self, book_id):
        """
        Retrieve html page
        book_id : goodreads book id number
        output : html string
        """

        url_root = 'https://www.goodreads.com/book/show/'

        req_session = HTMLSession()
        req = req_session.get(f'{url_root}{book_id}')

        return(req.html.html)

    def title_filt(self, title):
        """
        Keep only keywords in title
        title : string title
        output : string
        """
        # first remove parenthesis
        headtitle = re.sub(r'\(.*\)', '', title).lower()

        # remove stopwords
        stpwrds = set(stopwords.words('english'))
        keywrds = [wrd for wrd in headtitle.split() if wrd not in stpwrds]
        keytitle = ' '.join(keywrds)

        return(keytitle)

    def __fsearch(self, row):
        """
        formating search results
        """
        bbook = row['best_book']
        frow = {
            'goodreadsid' : bbook['id']['#text'],
            'title' : bbook['title'],
            'author' : bbook['author']['name'],
            'img_url' : bbook['image_url']
        }

        return(frow)


if __name__ == '__main__':
    from configparser import ConfigParser
    import calibre

    config = ConfigParser()
    # read config file
    config.read('parameters.ini')

    g = calibre.Reader('/mnt/alainsyno/Bibliothèque Calibre/Adrian Tchaikovsky/The Scarab Path (sota-5) (1389)/The Scarab Path (sota-5) - Adrian Tchaikovsky.epub')
    
    t = Talker(config['goodreads']['key'])

    # searchstr = '{} {}'.format(t.title_filt(g.title), g.writer)
    searchstr = g.writer

    search_res = t.search(searchstr)
    for res in search_res:
        t.get_info(res['goodreadsid'])

    print('end')