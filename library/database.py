# coding: utf8

from contextlib import closing

import logging
import json

logger = logging.getLogger(__name__)

class Bookcase(object):
    """
    An interface to postgresql database
    """
    
    def __init__(self, connector):
        self.connector = connector
        self.connector.autocommit = True
        self.curr_cursor = None

    def __iter__(self):
        """
        iterator initialisation
        """
        if self.curr_cursor is None:
            self.curr_cursor = self.connector.cursor()

            # retrieve column names
            sql = "SELECT column_name,data_type FROM information_schema.columns WHERE table_name = 'library';"
            self.curr_cursor.execute(sql)
            self.col_names, self.col_types = zip(*self.curr_cursor.fetchall())
            
            sql  = "SELECT * from library;"
            self.curr_cursor.execute(sql)
        return(self)

    def __next__(self):
        """
        iterator call, it explores the whole database one call, one row
        """
        result = self.curr_cursor.fetchone()
        if result is None: # end of database
            self.curr_cursor.close()
            self.curr_cursor = None

        return(dict(zip(self.col_names, result)))

    def insert(self, table='library', **book):
        """
        Insert one row into database with keywords as column names and arguments as values
        """
        
        keys = list(book.keys())
        vals = list(book.values())
            
        if table == 'library':
            keys.append('scraptime')
            vals.append('NOW()')

        keysf = ', '.join(keys)
        valsf = tuple(vals)

        nb_vals = f"{'%s, ' * len(valsf)}".strip(' ,')

        with closing(self.connector.cursor()) as cursor:
            sql  = f"INSERT INTO {table} ({keysf}) values ({nb_vals});"
            cursor.execute(sql, valsf)

            if table == 'library':
                logger.info(f"Inserted {book['writer_epub']} - {book['title_epub']}")
            elif table == 'spacy':
                logger.info(f"Inserted {book['id_library']} - chapter {book['part']}")

    def getbook(self, writer, title):
        """
        Select book
        """

        with closing(self.connector.cursor()) as cursor:
            sql  = f"""
            SELECT * FROM library WHERE writer_epub = %s AND title_epub = %s
            UNION ALL
            SELECT * , Null AS id_gr, Null AS title_gr, Null AS writer_gr, Null AS img_gr, Null AS html_gr, Null AS scraptime FROM nosummary WHERE writer_epub = %s AND title_epub = %s;
            """
            cursor.execute(sql, (writer, title)*2)
            result = cursor.fetchall()

        return(result)



if __name__ == '__main__':
    from configparser import ConfigParser

    import psycopg2

    config = ConfigParser()
    config.read('./configuration/parameters.ini')

    conn = psycopg2.connect(**config['postgresql'])

    libr = Bookcase(conn)

    libr.getbook('Stephen King', 'Case')

    print('testend')