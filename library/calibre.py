# coding: utf8

from bs4 import BeautifulSoup
from ebooklib import epub, ITEM_DOCUMENT
from langdetect import detect

import logging
import os
import nltk

logger = logging.getLogger(__name__)

class Reader(object):
    """
    Reader interact with epub ebooks
    """

    def __init__(self, path):
        """
        extract useful informations from the epub file
        path : string of the epub book pathfile
        """
        logger.debug(f'Unpack {path}')

        # entire epub
        self.path = path
        self.book = epub.read_epub(self.path)

        # metadata
        # for meta in ['identifier', 'language', 'title', 'creator']:
        #     setattr(self, meta, self.__meta_read(meta))
        self.id = self.__meta_read('identifier')
        self.lang = self.__meta_read('language')
        self.title = self.__meta_read('title')
        self.writer = self.__meta_read('creator')
        
      # extract text content
        self.chapters = []
        for item in self.book.get_items():
            if item.get_type() == ITEM_DOCUMENT:
                content = item.content
                soup = BeautifulSoup(content, 'html.parser')
                soup_text = soup.get_text()
                if soup_text.strip(): # don't keep blank documents
                    self.chapters.append(soup_text)

    def __repr__(self):
        return(f'Reader object of {self.path}')

    def __str__(self):
        return(f'Reader for epub {self.title} by {self.writer}')


    def __meta_read(self, meta):
        """
        private method to extract information from metadatas
        metadata : string of the metadata to read inside ['identifier', 'language', 'title', 'creator']
        output : only the first information
        """
        info_lst = self.book.get_metadata('DC', meta)
        info_filt = [info[0] for info in info_lst]

        if info_filt:
            info_rslt = info_filt[0]
        else:
            info_rslt = None
        return(info_rslt)

    def check_language(self):
        """
        method to check book language from chapter content
        output : language ISO 639-1 code
        """
        bgn = self.beginning()
        iso_lang = detect(self.chapters[bgn])
        return(iso_lang)

    def beginning(self):
        """
        method to get the first chapter with enough content
        output : chapter rating
        """
        frst_chp = None
        for i, chp in enumerate(self.chapters):
            tokens = nltk.wordpunct_tokenize(chp)
            if len(tokens) > 20: # arbitrary value
                frst_chp = i
                break

        if frst_chp is None:
            logger.info(f'No chapters found in {self.path}')

        return frst_chp


class Snooper(object):
    """
    Iterator for calibre library
    """

    def __init__(self, root):
        """
        root : root path file
        """
        self.root = root
    
    def __iter__(self):
        """
        return only desired english epub files
        """
        for loc, _dirs, files in os.walk(self.root):
            for f in files:
                f_path = f'{loc}/{f}'
                if '.epub' in f:
                    try:
                        f_reader = Reader(f_path)
                        if 'en' in (f_reader.lang, f_reader.check_language()):
                            yield(f_reader)
                    except Exception as e:
                        logger.exception(f'Reader failed for {f_path}\n{e}')
                        continue

    def distance_title(self, title1, title2):
        """
        Jaccard distance between title strings
        titile1 : first string to compare
        title2 : second string to compare
        output : numerical Jaccard distance
        """
        string1 = title1.lower()
        string2 = title2.lower()

        tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')

        wordset1 = set(tokenizer.tokenize(string1))
        wordset2 = set(tokenizer.tokenize(string2))

        distance = nltk.jaccard_distance(wordset1, wordset2)

        return(distance)
        


        

if __name__ == '__main__':
    from configparser import ConfigParser
    import logging.config

    config = ConfigParser()
    config.read('./configuration/parameters.ini')

    logging.config.fileConfig('./configuration/logging.ini')
    logger = logging.getLogger(__name__)

    snoopy = Snooper(config['calibre']['path'] + '/Ekaterina Sedia/')
    for f in snoopy:
        print(f, f.check_language(), f.lang)