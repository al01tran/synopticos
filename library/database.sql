CREATE TABLE IF NOT EXISTS library (
    id SERIAL,
    id_epub VARCHAR(500),
    path_epub  VARCHAR(500),
    title_epub VARCHAR(500),
    writer_epub VARCHAR(500),
    lang_epub VARCHAR(500),
    id_gr VARCHAR(500),
    title_gr VARCHAR(500),
    writer_gr VARCHAR(500),
    img_gr VARCHAR(500),
    html_gr TEXT,
    scraptime TIMESTAMP
);

CREATE TABLE IF NOT EXISTS nosummary (
    id SERIAL,
    id_epub VARCHAR(500),
    path_epub  VARCHAR(500),
    title_epub VARCHAR(500),
    writer_epub VARCHAR(500),
    lang_epub VARCHAR(500)
);

CREATE TABLE IF NOT EXISTS spacy (
    id SERIAL,
    id_library INTEGER,
    part INTEGER,
    dtype VARCHAR(20),
    shape0 INTEGER,
    shape1 INTEGER,
    vector TEXT,
    PRIMARY KEY (id_library, part)
);