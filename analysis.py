# coding: utf8

from configparser import ConfigParser
from library import calibre, goodreads, database

import psycopg2

# read config file
config = ConfigParser()
config.read('./configuration/parameters.ini')

# postgresql connection

conn = psycopg2.connect(**config['postgresql'])

db = database.Bookcase(conn)
gr_talker = goodreads.Talker(config['goodreads']['key'])
snoopy = calibre.Snooper(config['calibre']['path'])

path = "/mnt/alainsyno/Bibliothèque Calibre/Henry Kuttner/Don't Look Now (6492)/Don't Look Now - Henry Kuttner.epub"

reader = calibre.Reader(path)

print('end')